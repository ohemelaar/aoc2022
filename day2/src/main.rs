use std::fs;

enum OpponentMove {
    A,
    B,
    C,
}
enum PlayerMove {
    X,
    Y,
    Z,
}
struct Game {
    opponent: OpponentMove,
    player: PlayerMove,
}
fn parse_raw_game(input: &str) -> Game {
    Game {
        opponent: match input.chars().nth(0).unwrap() {
            'A' => OpponentMove::A,
            'B' => OpponentMove::B,
            'C' => OpponentMove::C,
            _ => panic!(),
        },
        player: match input.chars().nth_back(0).unwrap() {
            'X' => PlayerMove::X,
            'Y' => PlayerMove::Y,
            'Z' => PlayerMove::Z,
            _ => panic!(),
        },
    }
}
fn game_score(game: &Game) -> usize {
    (match (&game.opponent, &game.player) {
        (OpponentMove::A, PlayerMove::X)
        | (OpponentMove::B, PlayerMove::Y)
        | (OpponentMove::C, PlayerMove::Z) => 3,
        (OpponentMove::A, PlayerMove::Y)
        | (OpponentMove::B, PlayerMove::Z)
        | (OpponentMove::C, PlayerMove::X) => 6,
        _ => 0,
    } + match &game.player {
        PlayerMove::X => 1,
        PlayerMove::Y => 2,
        PlayerMove::Z => 3,
    })
}
fn second_game_score(game: &Game) -> usize {
    (match (&game.opponent, &game.player) {
        (OpponentMove::B, PlayerMove::X)
        | (OpponentMove::A, PlayerMove::Y)
        | (OpponentMove::C, PlayerMove::Z) => 1,
        (OpponentMove::C, PlayerMove::X)
        | (OpponentMove::B, PlayerMove::Y)
        | (OpponentMove::A, PlayerMove::Z) => 2,
        _ => 3,
    } + match &game.player {
        PlayerMove::X => 0,
        PlayerMove::Y => 3,
        PlayerMove::Z => 6,
    })
}
fn main() {
    let input = fs::read_to_string("input.txt").unwrap();
    // let input = fs::read_to_string("test.txt").unwrap();
    let strategy_guide: _ = input
        .lines()
        .map(|l| parse_raw_game(&l))
        .collect::<Vec<_>>();
    let total_score: usize = strategy_guide.iter().map(|g| game_score(g)).sum();
    println!("first star: {}", total_score);
    let second_total_score: usize = strategy_guide.iter().map(|g| second_game_score(g)).sum();
    println!("second star: {}", second_total_score);
}
