use std::{collections::HashSet, fs};

fn main() {
    // let input = fs::read_to_string("test.txt").unwrap();
    let input = fs::read_to_string("input.txt").unwrap();
    let mut rucksacks = vec![];
    for line in input.lines() {
        let left = &line[0..line.len() / 2];
        let right = &line[line.len() / 2..];
        rucksacks.push((left, right));
    }
    let sum_priorities: usize = rucksacks
        .iter()
        .map(|(l, r)| {
            str_to_char_set(l)
                .intersection(&str_to_char_set(r))
                .map(|c| char_to_priority(c))
                .sum::<usize>()
        })
        .sum();
    println!("first star: {}", sum_priorities);
    let sum_priorities_badges: usize = input
        .lines()
        .collect::<Vec<_>>()
        .chunks(3)
        .map(|chunk| {
            chunk
                .iter()
                .map(|bag| str_to_char_set(bag))
                .collect::<Vec<HashSet<char>>>()
                .into_iter()
                .reduce(|acc, itm| acc.intersection(&itm).copied().collect())
                .unwrap()
        })
        .map(|h| char_to_priority(h.iter().next().unwrap()))
        .sum();
    // .flatten()
    // .map(|h| char_to_priority(h))
    // .sum();
    // ;
    println!("second star: {}", sum_priorities_badges);
}

fn str_to_char_set(input: &str) -> HashSet<char> {
    input.chars().collect()
}

fn char_to_priority(input: &char) -> usize {
    let ascii = *input as usize;
    match ascii {
        64..=91 => ascii - 38,
        96..=122 => ascii - 96,
        _ => unimplemented!(),
    }
}
