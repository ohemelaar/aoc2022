use std::fs;
#[derive(Debug)]
struct Range {
    start: usize,
    end: usize,
}
impl Range {
    fn contains(&self, other: &Range) -> bool {
        self.start <= other.start && self.end >= other.end
    }
    fn overlaps(&self, other: &Range) -> bool {
        self.start <= other.start && self.end >= other.start
            || self.start <= other.end && self.end >= other.end
            || other.contains(self)
    }
}
#[derive(Debug)]
struct Pair {
    first: Range,
    second: Range,
}
impl Pair {
    fn self_contains(&self) -> bool {
        self.first.contains(&self.second) || self.second.contains(&self.first)
    }
    fn overlaps(&self) -> bool {
        self.first.overlaps(&self.second)
    }
}
fn main() {
    // let input = fs::read_to_string("test.txt").unwrap();
    let input = fs::read_to_string("input.txt").unwrap();
    let assignments = input
        .lines()
        .map(|raw| {
            let mut split = raw.split(',');
            let first_part = split.next().unwrap();
            let second_part = split.next().unwrap();
            Pair {
                first: Range {
                    start: first_part.split('-').next().unwrap().parse().unwrap(),
                    end: first_part.split('-').nth(1).unwrap().parse().unwrap(),
                },
                second: Range {
                    start: second_part.split('-').next().unwrap().parse().unwrap(),
                    end: second_part.split('-').nth(1).unwrap().parse().unwrap(),
                },
            }
        })
        .collect::<Vec<_>>();
    let first_star: usize = assignments
        .iter()
        .map(|a| if a.self_contains() { 1 } else { 0 })
        .sum();
    println!("first star: {}", first_star);
    let second_star: usize = assignments
        .iter()
        .map(|a| if a.overlaps() { 1 } else { 0 })
        .sum();
    println!("second star: {}", second_star);
}
