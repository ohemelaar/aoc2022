use std::fs;

fn main() {
    let input = fs::read_to_string("input.txt").unwrap();
    // let input = fs::read_to_string("test.txt").unwrap();
    let mut inventories: Vec<Vec<usize>> = vec![];
    let mut is_new_inv = true;
    for line in input.lines() {
        if line == "" {
            is_new_inv = true;
        } else {
            let value = line.parse().unwrap();
            if is_new_inv {
                inventories.push(vec![value]);
                is_new_inv = false;
            } else {
                inventories.last_mut().unwrap().push(value)
            }
        }
    }
    let mut inv_sums: Vec<usize> = inventories.iter().map(|inv| inv.iter().sum()).collect();
    inv_sums.sort();
    let max_inv_calories = inv_sums.iter().last().unwrap();
    println!("first star: {}", max_inv_calories);

    let three_most_sum: _ = inv_sums.iter().rev().take(3).sum::<usize>();
    println!("second star: {}", three_most_sum);
}
